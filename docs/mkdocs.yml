site_name: CMS ECAL automation documentation
site_description: Guidebook of the automation framework for the CMS ECAL calibration
site_author: Simone Pigazzini
site_url: https://ecalautomation.docs.cern.ch

repo_name: GitLab
repo_url: https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/
edit_uri: 'blob/master/docs'
docs_dir: 'source'

theme:
  name: material
  features:
    - navigation.sections
    - navigation.expand
    - navigation.top
    - toc.follow
    - toc.integrate
    - content.code.annotate

hooks:
  - hooks.py  

plugins:
  - search

markdown_extensions:
  - tables
  - attr_list
  - admonition
  - md_in_html
  - pymdownx.details
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.inlinehilite
  
nav:
  - Introduction: index.md  
  - For users:
      - General concepts: definitions.md
      - New workflow: new_wflow.md
      - Python tools: pytools.md
      - Shifterguide: shifterguide.md
  - For maintainers:
      - Application deployments: okd-deployments.md
      - Database deployment: influxdb.md
      - Reprocessing campaigns: data-reprocessing.md
  - Workflows:
      - Trivial Good Health Check:
          - Overview: workflows/cosmics-ghc/cosmics-ghc.md
          - Code docs: workflows/cosmics-ghc/build/html/index.html
      - Pulse shapes:
          - Overview: workflows/pulse-shapes/pulse-shapes.md
          - Code docs: workflows/pulse-shapes/build/html/index.html
      - Time calibration:
          - Overview: workflows/time-calibration/time-calibration.md
          - Code docs: workflows/time-calibration/build/html/index.html
      - PhiSym/EFlow:
          - Overview: workflows/phisym/phisym.md
          - Code docs: workflows/phisym/build/html/index.html            
      - ECALElf ntuples:
          - Overview: workflows/ecalelf-ntuples/ecalelf-ntuples.md
          - Code docs: workflows/ecalelf-ntuples/build/html/index.html
  - Utils:
      - Disk quotas monitoring:
          - Overview: workflows/quota-monitoring/quota-monitoring.md 
