#!/usr/bin/env groovy

def call(String script='', String image=env.image, String setup=env.image_setup) {
    withCredentials([usernamePassword(credentialsId: '[LXPLUS-NAME-SET-IN-JENKINS]', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        // Wrap the script into a singularity exec
        get_campaigns = "`if [ $campaign == 'all' ]; then ecalrunctrl.py --db $dbinstance list-campaigns -m; else echo $campaign | tr \",\" \"\\n\"; fi`"
        s_script = "apptainer exec -B /run -B /cvmfs -B /eos -B /afs $image /bin/bash -c 'echo '$PASSWORD' | kinit -V $USERNAME; export USER=$USERNAME; $setup; for CAMPAIGN in $get_campaigns \n do $script \n done'" 
        sh(script: s_script)
    }
}

