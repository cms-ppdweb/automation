#!/usr/bin/env groovy

def call(String script='', String image=env.image, String setup=env.image_setup) {
    withCredentials([usernamePassword(credentialsId: '[LXPLUS-NAME-SET-IN-JENKINS]', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        s_script = "apptainer exec -B /run -B /cvmfs -B /eos -B /afs $image /bin/bash -c 'echo '$PASSWORD' | kinit -V $USERNAME; export USER=$USERNAME; $setup; $script'"
        sh(script: s_script)
    }
}

