#!/bin/bash

CMSSW_REL=$1

# IMPORTANT NOTE:
# In the Gitlab CI you need to setup two global variables in Settings > CI/CD > Variables > Expand
# Add the following variables:
# 1. Key: 'SERVICE_USERNAME' - Value: '[the service account for the automation]'
# 2. Key: 'SERVICE_PWD' - Value: '[the Access Token generated in gitlab for the service account (see https://gitlab.cern.ch/help/user/profile/personal_access_tokens.md)]'

# exit when any command fails; be verbose
set -ex

# eos-client ("eos ls" and other stuff, not the actual deamon to mount the filesystem, that's done with the -B option by apptainer)
cat << EOF >>/etc/yum.repos.d/eos7-stable.repo
[eos8-stable]
name=EOS binaries from CERN Linuxsoft [stable]
gpgcheck=0
enabled=1
baseurl=http://linuxsoft.cern.ch/internal/repos/eos8-stable/x86_64/os
priority=9
EOF
dnf install --disablerepo epel-next -y --nogpgcheck eos-client

# openssl
dnf install --disablerepo epel-next -y --nogpgcheck openssl-libs openssl-devel

dnf install --disablerepo epel-next -y --nogpgcheck bc

# enable accessing T0 queues
dnf install --disablerepo epel-next -y --nogpgcheck environment-modules
mkdir -p /etc/modulefiles/lxbatch/
cat << EOF >> /etc/modulefiles/lxbatch/tzero
#%Module 1.0
# module file for lxbatch/share
#
setenv _myschedd_POOL tzero
setenv _condor_CONDOR_HOST "norwegianblue02.cern.ch, tzcm1.cern.ch"
EOF

# make cmsrel etc. work
shopt -s expand_aliases
source /cvmfs/cms.cern.ch/cmsset_default.sh

git config --global user.name '${SERVICE_USERNAME}'
git config --global user.email '${SERVICE_USERNAME}@cern.ch'
git config --global user.github '${SERVICE_USERNAME}'

mkdir -p /home/${SERVICE_USERNAME}/
cd /home/${SERVICE_USERNAME}/
cmsrel $CMSSW_REL 
cd $CMSSW_REL/src
cmsenv
git cms-init
cd -

# manually install the required packages needed for ecalautoctrl 
export PYTHON3PATH=$PYTHON3PATH:/home/${SERVICE_USERNAME}/lib/python3.9/site-packages/
git clone https://${SERVICE_USERNAME}:${SERVICE_PWD}@gitlab.cern.ch/cmsoms/oms-api-client.git
cd oms-api-client/
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I .
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I influxdb
python3 -m pip install --prefix /home/${SERVICE_USERNAME}/ -I dbs3-client>=4.0.19


# install ecalautoctrl
python3 -m pip install git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/ --prefix /home/${SERVICE_USERNAME}/ --no-deps -I

# copy lib and bin to the CMSSW area
mv /home/${SERVICE_USERNAME}/bin/* /home/${SERVICE_USERNAME}/$CMSSW_REL/bin/$SCRAM_ARCH/
mv /home/${SERVICE_USERNAME}/lib/python3.9/site-packages/* /home/${SERVICE_USERNAME}/$CMSSW_REL/python/

# compile
cd $CMSSW_BASE/src
cmsenv
scram b -j

# cmake (needed by EoP)
dnf install --disablerepo epel-next -y --nogpgcheck cmake 

# set w/r permissions on home directory
chmod 777 /home/${SERVICE_USERNAME}/

# setup script
cat << EOF >> /home/${SERVICE_USERNAME}/setup.sh
#!/bin/bash

source /usr/share/Modules/init/sh
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/${SERVICE_USERNAME}/$CMSSW_REL
cmsenv
cd -
EOF
